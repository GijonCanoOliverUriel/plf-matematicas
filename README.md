# Mapa conceptual 1
## Conjuntos, Aplicaciones y funciones (2002)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .caf {
    BackgroundColor #FFCAA6
  }
  .your_style_name {
    BackgroundColor lightblue
  }
}
</style>
* Conjuntos, Aplicaciones y funciones (2002) <<rose>>
	*_ las
		* Matemáticas <<green>>
			*_ son
				* capaz de encontrar propiedades válidas
					*_ no 
						* tener que ir demostrando caso por caso
			*_ una 
				* de sus vocaciones no es solo dar soluciones a problemas
			*_ está 
				* muy cerca de la filosofía
					*_ por
						* conjuntos, aplicaciones y la lógica proposicional
							*_ son 
								* fruto de ideas
									*_ de
										* los matemáticos 
										* los filósofos
	*_ los 
		* Conjunto y elemento <<green>>
			*_ Son 
				* Conceptos primitivos
					*_ son 
						* en este ámbito los únicos conceptos que no se define
							* otra cosa 
								* que no se define es la relación que hay entre ellos
					* no se definen 
						*_ porque 
							* cualquier definición sería similar
	*_ el 
		* conjunto <<green>>
			* es
				* la agrupacion de diferentes elementos
			* cardinal de un conjunto
				* es 
					* el número de elementos 
						*_ que 
							* conforman el conjunto
				* acotación de cardinales
					*_ se 
						* basaba en esta misma fórmula 
						* pueden encontrar una serie de relaciones 
			*_ se 
				* suelen representante mediante dibujos 
					*_ que
						* se denominados diagramas de venn
			* la inclusión de conjuntos
				*_ es 
					* un trasunto de la relación de orden de números 
						*_ en 
							* conjunto con otros elementos 
								*_ que 
									* pertenecen al segundo
			* la relación de pertenencia
				*_ es 
					* un elemento y un conjunto
						*_ que 
							* siempre sepuede saber si el elemento está o no está dentro del conjunto
			* conjuntos  universal
				* es un conjunto de referencia 
					*_ en 
						* el que ocurre toda sociedad teoríca
				* el vacío es una necesidad 
					*_ también 
						* es una necesidad lógica 
							* para cerrar bien las cosas
	*_ una
		* aplicación <<green>>
			*_ es
				* una transformación
					* el primero elemento 
						*_ se 
							* convierte en el segundo
					*_ si
						* se olvida de uno de los elementos del conjunto
							*_ no 
								* es aplicación en los términos matemáticos
				* una regla 
					*_ que 
						* convierte a cada uno de los elementos de un determinado conjunto
			*_ los 
				* tipos de aplicaciones
					*_ son
						* aplicaciones inyectiva
						* aplicaciones sobreyectiva
						* aplicaciones viyectiva
	*_ las
		* aplicaciones y funciones <<green>>
			*_ ejemplos
				* la biología nos ha enseñado Cómo han evolucionado los seres vivos
				* los agentes económicos influyen en los niveles de vida
				* situaciones en las decisiones del gobierno
	*_ una 
		* función <<green>>
			*_ es
				* cuando se tiene una transformación 
				* una aplicación entre conjuntos de números
@endmindmap
```

# Mapa conceptual 2
## Funciones (2010)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .caf {
    BackgroundColor #FFCAA6
  }
  .your_style_name {
    BackgroundColor lightblue
  }
}
</style>
* Funciones (2010) <<green>>
	*_ es 
		* un reflejo, el pensamiento  <<your_style_name>>
			*_ que
				* el hombre ha hecho para intentar comprender su entorno
			*_ para 
				* intentar con ayuda las matemáticas 
					*_ resolver 
						* sus problemas cotidianos
		* una aplicación especial <<your_style_name>>
			*_ que 
				* los conjunto se Están relacionando 
					*_ tienen 
						* representaciones gráficas
					* son conjuntos de números 
						*_ más 
							* en particular el conjunto de los números reales
	*_ pueden
		* ser tambien gráficas con líneas rectas <<your_style_name>>
			*_ que
				* se llaman funciones lineales
	*_ la
		* la idea de función la más sencilla <<your_style_name>>
			*_ una 
				* función que a cada número le asigna su cuadrado
					*_ quiere decir 
						* que es una regla al número 1 le asigna su cuadrado
					*_ En
						* otro conjunto de números esta regla se dice se toma el número
							*_ se 
								* eleva al cuadrado y se obtiene su imagen
									*_ eso 
										* es simplemente una función
	*_ tiene
		* continuidad <<your_style_name>>
			*_ es 
				* una función que tiene buen comportamiento
					*_ no produce
						* saltos
						*  discontinuidades
					*_ son 
						* funciones más manejables
		* límite <<your_style_name>>
			*_ es
				* su comportamiento local
			*_ tiene 
				* la particularidad que encima es continua
			*_ es 
				* cómo se aproxima
					*_ a los 
						* valeros de una función cuando nos acercamos al punto de interésa 
							*_ Qué 
								* valor se aproxima los baleros de una función 
									*_ cuando 
										* nos acercamos al punto de interés
	*_ la
		* derivada <<your_style_name>>
			*_ es
				* resolver el problemade 
				* la aproximación de una función compleja
					*_ mediante 
						* una función lineal 
						* una función simple
							*_ esa 
								* es la motivación de la derivada 
									*_ así 
										* nace la derivada
	*_ puede ser
		* crecientes <<your_style_name>>
			*_ son 
				* crecientes cuando aumenta la Variable 
					*_ que 
						* se llama independiente o \nvariable del primer conjunto 
							*_ los 
								* valores del primer conjunto aumentan sus imágenes
		* decrecientes <<your_style_name>>
			*_ es 
				* cuando al aumentar las x 
				* disminuyen las imágenes
@endmindmap
```



# Mapa conceptual 3
## La matemática del computador (2002)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .caf {
    BackgroundColor #FFCAA6
  }
  .your_style_name {
    BackgroundColor lightblue
  }
}
</style>
* La matemática del computador (2002) <<caf>>
	*_ tiene 
		* 5 Problemas mas comunes <<rose>>
			*_ son
				* La idea de error
					*_ se 
						* comete al tener que aproximar un número real
							*_ a
								* una numero con una cantidad infinita de cifras
				* La idea de dígitos significativos
					* describen una cantidad
				* La idea de números aproximados
				* Truncar un número
					*_ es 
						* cortar un numero 
							*_ unas 
								* cuantas cifras a la derecha 
									*_ por ejemplo 
										* π = 3.14
										* e = 2.718
				* Redondear un número
					*_ es 
						* intentar que el error cometido 
							*_ sea 
								* lo menor posible
	*_ cuenta con
		* sistemas de numeración <<rose>>
			*_ son
				* octal
					* base 8
				* hexadecimal
					* base 16
				* binario
					* base 2
					*_ se 
						* representa por 0, 1
						* usa tambien 
							*_ en 
								* la electrónica
									*_ como
										* un interruptor
											* 1 es encendido
											* 0 es apagado
@endmindmap
```

